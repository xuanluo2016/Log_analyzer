
JDBC log analyzer

Usage: JDBC log analysis. 

Example: python loganalyzer.py -f jdbc.log

Output: jdbc.xlsx (By default logs are ordered by the total running time of a query)
